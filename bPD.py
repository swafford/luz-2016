#!/usr/bin/env python


#developed for windows 7, python 2.x. 
#Email Andrew.swafford@lifesci.ucsb.edu with questions or comments.

from ete2 import *
import csv
import argparse
import random
import numpy
import sys
from scipy import stats

def phylodiv(samples,sampleSize,bootstraps,treeFile,root,output):
    sampleDict = {}
    searchList = []
    tree = Tree(treeFile)
    ctree = tree.get_cached_content()
    with open(samples) as CSVfile: #open and read in groups + samples
        field_names = ['group','taxon']
        file_in = csv.DictReader(CSVfile,fieldnames = field_names,delimiter = "\t")
        for row in file_in:
            sampleDict.setdefault(row['group'],[])
            sampleDict[row['group']].append(row['taxon'])
    pickList = []
    tList = []
    with open("{0}.csv".format(output),'w') as OUTFILE: #open output file & prep for writing.
        OUTFILE.write("{3},{0},{1},{2}\n".format('PD (mean)','Upper CI','Lower CI','Group')) #headers in output.
        for entry in sampleDict.keys():
            pickList = []
            if len(sampleDict[entry]) > sampleSize: #check if group has more samples than sample size.
                while len(pickList) < bootstraps:   #check if number of BS is met.
                    while len(tList) < sampleSize:  #check if number of samples for this replicate has been met.
                        randPick = random.randrange(0,len(sampleDict[entry]))
                        if not sampleDict[entry][randPick] in tList: #pick a random sample from group that is not already picked.
                            tList.append(sampleDict[entry][randPick])
                    pickList.append(tList)
                    tList = []
            else:
                pickList = sampleDict[entry]        #add all samples to PD calc if site is smaller than sample size.
            pds = pdCalc(pickList,tree,root)        #calculate PDs
            print pds                               #show progression
            if any(isinstance(el, tuple) for el in pds) == True:  #write to outfile
                OUTFILE.write("{3},{0},{1},{2}\n".format(pds[0],pds[1][1],pds[1][0],entry))
            else:
                OUTFILE.write("{3},{0},{1},{2}\n".format(pds[0],0,0,entry))
    

def pdCalc(samplist,tree,includeRoot,bootstrapping = True):
    if any(isinstance(el, list) for el in samplist) == False: #see if ALL samples are added or if we're bootstrapping a larger site.
        samplist = [samplist]
        bootstrapping = False
    x=0
    pd_list = []
    for cohort in samplist: #test the PD for each bootstrap replicate
        pd_value = 0
        taxalist = cohort
        mrca = tree.get_common_ancestor(taxalist)
        path = []
        for leaf in taxalist:
            node = tree&leaf
            while node.up:
                if node not in path:
                    path.append(node)
                    pd_value = node.dist + pd_value
                node = node.up
                if node == mrca:
                    break
        if includeRoot == True:
            while node.up:
                if node not in path:
                    path.append(node)
                    pd_value = node.dist + pd_value
                node = node.up
        pd_list.append(pd_value)
    if bootstrapping == True:
        arr = numpy.array(pd_list)
        mean = numpy.mean(arr)
        stdev = numpy.std(arr)
        conf_int = stats.norm.interval(0.95, loc=mean, scale=stdev / numpy.sqrt(len(samplist))) #calculate our distribution of PDs
        return([mean,conf_int])
    else:
        return(pd_list)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Calculate & bootstrap PDs")
    parser.add_argument("-i", help='Tabular file in the format "Group    Taxon".',
                        required = True)
    parser.add_argument("-t", help='Tree file in newick format.',
                        required = True)
    parser.add_argument("-b", help = "Number of Boostrap replicates (i.e. random samples) to perform",
                        required = True, type=int)
    parser.add_argument("-s", help = "Sample size to sample from each group. If s > # samples in group, all samples will be considered.",
                        required = True,type=int)
    parser.add_argument("-r", help = "Include the root in calculations. default False.",
                        default = False, choices = [True,False], type=bool)
    parser.add_argument("-o", help = "Output file",
                        default = "bPD_output")
    args = parser.parse_args()
    phylodiv(args.i,args.s,args.b,args.t,args.r,args.o)
