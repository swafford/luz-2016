import ete3
from ete3 import Tree
from ete3 import NCBITaxa
ncbi = NCBITaxa()

def recursive_place(node,lineage): #places nodes based on lineage
    placedict = {} 
    for child in node.get_children():
        cName = " ".join(child.get_leaves()[0].name.split("_"))
        cLin = ncbi.get_lineage(ncbi.get_name_translator([cName])[cName][0])
        cLin = cLin[0:len(lineage)]
        placedict.setdefault(child,cLin)
    i=0
    for key in placedict.keys():
        x = set(placedict[key]).intersection(set(lineage))
        x=len(x)
        if x == i:
            i=x
            choice = node
        if x > i:
            i = x
            choice = key
    if i == 0:
        print "MRCA",node
        print "LINEAGE",lineage
        print node.get_children()
        print node.dist
        choice = node
    return(choice)
        

aBplaced = "needed_tips_3.txt" #file with names of tips to be placed, separated by newlines
tree2 = "R_pruned_tree_round2" #tree to place tips on in newick notation
outtree = "output.tree" #name of the output file
outgroup = "Pollia_macrophylla,Gigantochloa_scortechinii,Chusquea_culeou,Prestoea_acuminata,Chusquea_quila"#outgroup

still_missing = []
tree = Tree(tree2)
with open(aBplaced,'r') as place:
    x = place.read().split("\n")
    missing = list(set(x)-set(tree.get_tree_root().get_leaf_names()))
    print "Number to be added: ", len(missing)
    for line in missing:
        geList = []
        if len(line) < 4:
            continue
        print "placing ",line
        genus = line.split("_")[0]
        species = line.split("_")[1]
        for taxa in tree.iter_leaves():
            if taxa.name.split("_")[0] == genus:
                geList.append(taxa)
        if len(geList) == 0:
            print "Genus not found, searching family"
            still_missing.append(line)
            ncbID = ncbi.get_name_translator([genus])[genus]
            lineage = ncbi.get_lineage(ncbID[0])
            ranks = ncbi.get_rank(lineage)
            for rankID in ranks.keys():
                if ranks[rankID] == u'family':
                    familyID = ncbi.get_taxid_translator([rankID])[rankID]
                    for node in tree.traverse(strategy="postorder"):
                        if node.name == str(familyID):
                            placedict = {}
                            rnode = recursive_place(node,lineage)
                            while rnode != node:
                                node = rnode
                                rnode = recursive_place(node,lineage) 
                            print "Family found & placed"
                            mrca = rnode    
                            if len(mrca.get_children()) == 0:
                                print "Closest Family was OTU, placing as sister"
                                blen = mrca.dist
                                mrca.up.add_child(name=line,dist=blen)
                            else:
                                blen = mrca.get_distance(mrca.get_leaves()[0])
                                mrca.add_child(name=line,dist=blen)
                            still_missing.remove(line)
                    if line in still_missing:
                        x = 0
                        i = 0
                        choice = 0
                        for node in tree.iter_leaves():
                            nodeID = ncbi.get_name_translator([node.name.split("_")[0]])
                            nodeLin = ncbi.get_lineage(nodeID[node.name.split("_")[0]][0])
                            x = set(nodeLin).intersection(set(lineage))
                            x=len(x)
                            if x > i:
                                i = x
                                choice = node
                        if i !=0:
                            print "Discovered closeset relative as OTU, placing as sister."
                            blen = choice.dist
                            choice.up.add_child(name=line,dist=blen)
            if line == "Emmotum_nitens": #certain genera are problematic, manually assign their location
                mrca = tree.get_common_ancestor("Fraxinus_profunda","Plocosperma_buxifolium")
                blen = mrca.get_distance("Fraxinus_profunda")
                mrca.add_child(name = line, dist = blen)
                print "added Emmotum"
        else:
            if len(geList)==1:
                print "Only one species in genus, placing as sister"
                mrca = geList[0].up
            else:
                print "Genus found, placing at MRCA"
                mrca = tree.get_common_ancestor(geList)
            blen = mrca.get_distance(geList[0])#split branch lengths on new node
            mrca.add_child(name=line,dist=blen)
tree.write(outfile = outtree,format=1)
aBplaced = "missing_taxa_iter2"
with open(aBplaced,'w') as iter2:
    for i in still_missing:
        iter2.write("{0}\n".format(i))

outgroup = tree.get_common_ancestor(outgroup.split(","))
#print "Pruning, this may take several minutes..."
#tree.prune(x,preserve_branch_length=True)


tree.write(outfile = "Pruned_tree", format = 1)
print"pruned and saved..randomly resolving polytomies because R can't handle itself"
tree.resolve_polytomy(recursive=True)
#print"rooting at user defined outgroup because R is a little baby"
#tree.set_outgroup(outgroup)
for zeroNode in tree.search_nodes(dist=0):
    zeroNodeChildren = zeroNode.get_children()
    if zeroNode.is_leaf() == True:
        print "LEAF",zeroNode
    if len(zeroNodeChildren) > 1 and zeroNode.is_root() == False:
        smallNode = min([zero.dist for zero in zeroNodeChildren])/2
        print "Min node dist: ",smallNode
        for node in zeroNodeChildren:
            node.dist = node.dist - smallNode
        zeroNode.dist = smallNode
        
            
        
        
tree.write(outfile = "%s_tree2" % outtree)




            
